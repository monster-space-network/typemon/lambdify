//
//
//
export enum MetadataKey {
    ControllerMetadata = 'typemon:lambdify:controller-metadata',
    HandlerMetadatas = 'typemon:lambdify:handler-metadatas',
    MiddlewareMetadata = 'typemon:lambdify:middleware-metadata'
}
