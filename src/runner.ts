import { Constructor, TargetHandle, Container, Lifetime, Provider, InstanceHandle, ParameterMetadata } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
import Boom from '@hapi/boom';
//
import { MetadataKey } from './metadata-key';
import { GlobalOptions } from './global-options';
import { Event, Context, ParameterToken, HttpRequest, HttpSharedContext, ParameterResolver, HttpResponse } from './parameters';
import { Controller } from './controller';
import { Handler } from './handler';
import { Middleware } from './middleware';
//
//
//
export class Runner {
    private readonly targetHandle: TargetHandle;

    private readonly controllerMetadata: Controller.Metadata;
    private readonly handlerMetadata: Handler.Metadata;

    private readonly useAPIGateway: boolean;

    private done: boolean;

    public constructor(
        private readonly target: Constructor,
        private readonly propertyKey: string,
        private readonly globalOptions: GlobalOptions
    ) {
        this.targetHandle = TargetHandle.create(this.target);

        this.controllerMetadata = this.targetHandle.getMetadata(MetadataKey.ControllerMetadata);
        this.handlerMetadata = this.targetHandle.getMetadata(MetadataKey.HandlerMetadatas).get(this.propertyKey);

        this.useAPIGateway = [
            this.handlerMetadata.options.useAPIGateway,
            this.controllerMetadata.options.useAPIGateway,
            this.globalOptions.useAPIGateway
        ].every((useAPIGateway: undefined | boolean): boolean => Check.isUndefined(useAPIGateway) || Check.isTrue(useAPIGateway));

        this.done = false;
    }

    private createRootContainer(event: Event, context: Context): Container {
        const container: Container = Container.create();

        container.bindAll(this.globalOptions.providers || []);
        container.bindAll(this.controllerMetadata.options.providers || []);
        container.bindAll([
            {
                identifier: ParameterToken.EVENT,
                useValue: event,
                lifetime: Lifetime.Singleton
            },
            {
                identifier: ParameterToken.CONTEXT,
                useValue: context,
                lifetime: Lifetime.Singleton
            }
        ]);

        if (Check.isFalse(this.useAPIGateway)) {
            return container;
        }

        container.bindAll([
            HttpRequest.PROVIDER,
            HttpResponse.PROVIDER,
            HttpSharedContext.PROVIDER
        ]);

        return container;
    }
    private createHandlerContainer(rootContainer: Container): Container {
        const container: Container = Container.create({
            parent: rootContainer
        });

        container.bindAll(this.handlerMetadata.options.providers || []);

        return container;
    }

    private async executeMiddleware(middleware: Middleware.Constructor | Middleware.WithProviders, parentContainer: Container): Promise<void> {
        const [target, providers]: [Constructor, ReadonlyArray<Provider>] = Middleware.WithProviders.isWithProviders(middleware)
            ? [middleware.target, middleware.providers]
            : [middleware, []];
        const targetHandle: TargetHandle = TargetHandle.create(target);
        const metadata: Middleware.Metadata = targetHandle.getMetadata(MetadataKey.MiddlewareMetadata);
        const container: Container = Container.create({
            parent: parentContainer
        });
        let next: boolean = false;

        container.bindAll(metadata.options.providers || []);
        container.bindAll(providers);
        container.bind({
            identifier: ParameterToken.NEXT,
            useValue: (): void => {
                next = true;
            },
            lifetime: Lifetime.Singleton
        });

        const instance: object = await container.resolve(target);
        const instanceHandle: InstanceHandle = InstanceHandle.create(instance);
        const parameterMetadatas: ReadonlyArray<ParameterMetadata> = instanceHandle.getParameterMetadatas('handler');
        const parameters: ReadonlyArray<unknown> = await ParameterResolver.resolveAll(parameterMetadatas, container);

        await instanceHandle.callProperty('handler', parameters);

        if (Check.isFalse(next)) {
            this.done = true;
        }
    }
    private async executeMiddlewares(middlewares: Iterable<Middleware.Constructor | Middleware.WithProviders>, parentContainer: Container): Promise<void> {
        for (const middleware of middlewares) {
            await this.executeMiddleware(middleware, parentContainer);

            if (Check.isTrue(this.done)) {
                return;
            }
        }
    }
    private async executeHandler(container: Container): Promise<unknown> {
        const instance: object = await container.resolve(this.target);
        const instanceHandle: InstanceHandle = InstanceHandle.create(instance);
        const parameterMetadatas: ReadonlyArray<ParameterMetadata> = instanceHandle.getParameterMetadatas(this.propertyKey);
        const parameters: ReadonlyArray<unknown> = await ParameterResolver.resolveAll(parameterMetadatas, container);
        const result: unknown = await instanceHandle.callProperty(this.propertyKey, parameters);

        return result;
    }

    public async run(event: Event, context: Context): Promise<unknown> {
        const rootContainer: Container = this.createRootContainer(event, context);
        const handlerContainer: Container = this.createHandlerContainer(rootContainer);
        const tasks: Array<() => Promise<unknown>> = [];

        if (this.useAPIGateway) {
            tasks.push(
                (): Promise<void> => this.executeMiddlewares(this.globalOptions.middlewares || [], rootContainer),
                (): Promise<void> => this.executeMiddlewares(this.controllerMetadata.options.middlewares || [], rootContainer),
                (): Promise<void> => this.executeMiddlewares(this.handlerMetadata.options.middlewares || [], handlerContainer)
            );
        }

        tasks.push((): Promise<unknown> => this.executeHandler(handlerContainer));

        try {
            const result: unknown = await tasks.reduce(async (promise: Promise<unknown>, nextTask: () => Promise<unknown>): Promise<unknown> => promise.then((result: unknown): unknown | Promise<unknown> => {
                if (Check.isTrue(this.done)) {
                    return result;
                }

                return nextTask();
            }), Promise.resolve());

            if (Check.isFalse(this.useAPIGateway)) {
                return result;
            }

            const response: HttpResponse = await rootContainer.get(HttpResponse);

            response.statusCode = this.handlerMetadata.options.statusCode || response.statusCode;

            for (const [name, value] of Object.entries(this.handlerMetadata.options.headers || {})) {
                response.headers.append(name, value);
            }

            if (Check.isNotUndefined(result)) {
                response.body = result;
            }

            const serializedResponse: HttpResponse.Serialized = HttpResponse.serialize(response);

            return serializedResponse;
        }
        catch (error) {
            if (Check.isFalse(this.useAPIGateway)) {
                return error;
            }

            const boom: Boom = Boom.isBoom(error)
                ? error
                : Boom.internal();
            const serializedResponse: HttpResponse.Serialized = HttpResponse.serialize({
                statusCode: boom.output.statusCode,
                headers: boom.output.headers,
                body: {
                    statusCode: boom.output.payload.statusCode,
                    error: boom.output.payload.error,
                    message: boom.output.payload.message,
                    data: boom.data
                }
            });

            return serializedResponse;
        }
    }
}
