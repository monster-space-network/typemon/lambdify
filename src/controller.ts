import { Provider, Constructor, TargetHandle } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
import { Middleware } from './middleware';
//
//
//
export function Controller(options: Controller.Options = {}): ClassDecorator {
    return (target: Function): void => {
        const constructor: Constructor = TargetHandle.targetify(target);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);

        if (targetHandle.hasMetadata(MetadataKey.ControllerMetadata)) {
            throw new Error(`Decorator 'Controller' can not be duplicated.`);
        }

        const metadata: Middleware.Metadata = {
            options
        };

        targetHandle.setMetadata(MetadataKey.ControllerMetadata, metadata);
    };
}
export namespace Controller {
    export interface Options {
        /**
         * @default true
         */
        readonly useAPIGateway?: boolean;
        readonly providers?: ReadonlyArray<Provider>;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor | Middleware.WithProviders>;
    }
    export interface Metadata {
        readonly options: Options;
    }
}
