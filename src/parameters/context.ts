import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
import Lambda from 'aws-lambda';
//
import { ParameterToken } from './parameter-token';
//
//
//
export type Context = Lambda.Context;
export function Context(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Context' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Context' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.CONTEXT, target, propertyKey, index);
    };
}
