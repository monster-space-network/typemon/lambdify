import { Injectable, Provider, Lifetime, Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
import { APIGatewayEvent } from 'aws-lambda';
//
import { ParameterToken } from '../parameter-token';
import { Event } from '../event';
import { HttpSharedContext } from './http-shared-context';
import { HttpHeaders, ReadonlyHttpHeaders } from './http-headers';
import { HttpParameters, ReadonlyHttpParameters } from './http-parameters';
//
//
//
@Injectable()
export class HttpRequest<Body = any> {
    public static readonly PROVIDER: Provider = {
        identifier: HttpRequest,
        useClass: HttpRequest,
        lifetime: Lifetime.Singleton
    };

    public readonly headers: ReadonlyHttpHeaders;
    public readonly pathParameters: ReadonlyHttpParameters;
    public readonly queryStringParameters: ReadonlyHttpParameters;
    public readonly body: Body;

    public constructor(
        public readonly sharedContext: HttpSharedContext,
        @Inject(ParameterToken.EVENT) private readonly event: Event<APIGatewayEvent>
    ) {
        this.headers = new HttpHeaders(this.event.headers);
        this.pathParameters = new HttpParameters(this.event.pathParameters || {});
        this.queryStringParameters = new HttpParameters(this.event.queryStringParameters || {});
        this.body = Check.isString(this.event.body)
            ? JSON.parse(this.event.body)
            : null;
    }

    public get method(): string {
        return this.event.httpMethod;
    }

    public get path(): string {
        return this.event.path;
    }
}
