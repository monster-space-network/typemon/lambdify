import { Injectable, Provider, Lifetime } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
//
//
@Injectable()
export class HttpSharedContext {
    public static readonly PROVIDER: Provider = {
        identifier: HttpSharedContext,
        useClass: HttpSharedContext,
        lifetime: Lifetime.Singleton
    };

    private readonly map: Map<string, unknown>;

    public constructor() {
        this.map = new Map();
    }

    public has(key: string): boolean {
        const has: boolean = this.map.has(key);

        return has;
    }
    public hasNot(key: string): boolean {
        const has: boolean = this.has(key);
        const hasNot: boolean = Check.isFalse(has);

        return hasNot;
    }

    public get(key: string): any {
        if (this.hasNot(key)) {
            throw new Error(`Key '${key}' does not exist.`);
        }

        const value: unknown = this.map.get(key);

        return value;
    }

    public set(key: string, value: unknown): void {
        this.map.set(key, value);
    }

    public delete(key: string): void {
        this.map.delete(key);
    }
}
