import { Check } from '@typemon/check';
//
//
//
export class HttpParameters {
    private readonly map: Map<string, string>;

    public constructor(parameters: Readonly<Record<string, string>> = {}) {
        this.map = new Map();

        for (const [key, value] of Object.entries(parameters)) {
            this.set(key, value);
        }
    }

    public has(key: string): boolean {
        const has: boolean = this.map.has(key);

        return has;
    }
    public hasNot(key: string): boolean {
        const has: boolean = this.has(key);
        const hasNot: boolean = Check.isFalse(has);

        return hasNot;
    }

    public get(key: string): string {
        if (this.hasNot(key)) {
            throw new Error(`Key '${key}' does not exist.`);
        }

        const value: string = this.map.get(key) as string;

        return value;
    }

    public set(key: string, value: string): void {
        this.map.set(key, value);
    }

    public delete(key: string): void {
        this.map.delete(key);
    }
    public deleteAll(keys: Iterable<string> = this.map.keys()): void {
        for (const key of keys) {
            this.delete(key);
        }
    }
}
export interface ReadonlyHttpParameters {
    has(key: string): boolean;
    hasNot(key: string): boolean;
    get(key: string): string;
}
