import { Injectable, Provider, Lifetime } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { HttpSharedContext } from './http-shared-context';
import { HttpHeaders, ReadonlyHttpHeaders } from './http-headers';
//
//
//
@Injectable()
export class HttpResponse {
    public static readonly PROVIDER: Provider = {
        identifier: HttpResponse,
        useClass: HttpResponse,
        lifetime: Lifetime.Singleton
    };

    /**
     * @default 200
     */
    public statusCode: number;

    public readonly headers: HttpHeaders;

    /**
     * @default null
     */
    public body: any;

    public constructor(
        public readonly sharedContext: HttpSharedContext
    ) {
        this.statusCode = 200;
        this.headers = new HttpHeaders();
        this.body = null;
    }
}
export namespace HttpResponse {
    export interface Serialized {
        readonly statusCode: number;
        readonly headers: Readonly<Record<string, string>>;
        readonly body: string;
    }

    export function serialize({ statusCode, headers, body }: {
        readonly statusCode: number;
        readonly headers: Readonly<Record<string, string>> | ReadonlyHttpHeaders;
        readonly body: unknown;
    }): Serialized {
        const serializedHeaders: Record<string, string> = {};
        const headerEntries: Iterable<[string, string]> = headers instanceof HttpHeaders
            ? headers
            : Object.entries(headers);

        for (const [name, value] of headerEntries) {
            serializedHeaders[name] = value;
        }

        const serializedBody: string = Check.isNotEmpty(body)
            ? Check.isString(body)
                ? body
                : JSON.stringify(body)
            : '';
        const serializedResponse: Serialized = {
            statusCode,
            headers: serializedHeaders,
            body: serializedBody
        };

        return serializedResponse;
    }
}
