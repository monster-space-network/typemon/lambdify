import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { HttpRequest } from '../http-request';
//
//
//
export type Request = HttpRequest;
export function Request(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Request' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Request' can not be used for symbol properties.`);
        }

        Inject.decorate(HttpRequest, target, propertyKey, index);
    };
}
