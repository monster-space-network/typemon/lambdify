import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { ParameterToken } from '../../parameter-token';
import { HttpResponse } from '../http-response';
import { HttpHeaders } from '../http-headers';
//
//
//
export type Response = HttpResponse;
export function Response(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Response' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Response' can not be used for symbol properties.`);
        }

        Inject.decorate(HttpResponse, target, propertyKey, index);
    };
}
export namespace Response {
    export type Headers = HttpHeaders;
    export function Headers(): ParameterDecorator {
        return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
            if (Check.isUndefined(propertyKey)) {
                throw new Error(`Decorator 'Response.Headers' can not be used in the constructor.`);
            }

            if (Check.isSymbol(propertyKey)) {
                throw new Error(`Decorator 'Response.Headers' can not be used for symbol properties.`);
            }

            Inject.decorate(ParameterToken.RESPONSE_HEADERS, target, propertyKey, index);
        };
    }
}
