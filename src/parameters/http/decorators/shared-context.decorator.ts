import { Inject, Constructor, TargetHandle, ParameterMetadata } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { MetadataKey } from '../../metadata-key';
import { HttpSharedContext } from '../http-shared-context';
//
//
//
export type SharedContext = HttpSharedContext;
export function SharedContext(key?: string): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'SharedContext' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'SharedContext' can not be used for symbol properties.`);
        }

        Inject.decorate(HttpSharedContext, target, propertyKey, index);

        if (Check.isUndefined(key)) {
            return;
        }

        const constructor: Constructor = TargetHandle.targetify(target, propertyKey);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);
        const metadata: ParameterMetadata = targetHandle.getParameterMetadata(index, propertyKey);

        metadata.set(MetadataKey.SharedContextKey, key);
    };
}
