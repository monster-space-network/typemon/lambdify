import { Inject, Constructor, TargetHandle, ParameterMetadata } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { MetadataKey } from '../../metadata-key';
import { ParameterToken } from '../../parameter-token';
//
//
//
export function Header(name: string): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Header' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Header' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.HEADER, target, propertyKey, index);

        const constructor: Constructor = TargetHandle.targetify(target, propertyKey);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);
        const metadata: ParameterMetadata = targetHandle.getParameterMetadata(index, propertyKey);

        metadata.set(MetadataKey.HeaderName, name);
    };
}
