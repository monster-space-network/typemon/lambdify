import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { ParameterToken } from '../../parameter-token';
import { ReadonlyHttpParameters } from '../http-parameters';
//
//
//
export type QueryParameters = ReadonlyHttpParameters;
export function QueryParameters(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'QueryParameters' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'QueryParameters' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.QUERY_PARAMETERS, target, propertyKey, index);
    };
}
