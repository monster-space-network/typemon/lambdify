import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { ParameterToken } from '../../parameter-token';
import { ReadonlyHttpHeaders } from '../http-headers';
//
//
//
export type Headers = ReadonlyHttpHeaders;
export function Headers(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Headers' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Headers' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.HEADERS, target, propertyKey, index);
    };
}
