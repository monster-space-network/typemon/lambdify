import { Inject, Constructor, TargetHandle, ParameterMetadata } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { MetadataKey } from '../../metadata-key';
import { ParameterToken } from '../../parameter-token';
import { ReadonlyHttpParameters } from '../http-parameters';
//
//
//
export type PathParameter = ReadonlyHttpParameters;
export function PathParameter(key: string): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'PathParameter' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'PathParameter' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.PATH_PARAMETER, target, propertyKey, index);

        const constructor: Constructor = TargetHandle.targetify(target, propertyKey);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);
        const metadata: ParameterMetadata = targetHandle.getParameterMetadata(index, propertyKey);

        metadata.set(MetadataKey.ParameterKey, key);
    };
}
