import { Check } from '@typemon/check';
//
//
//
export class HttpHeaders implements Iterable<[string, string]> {
    private readonly map: Map<string, string>;

    public constructor(headers: Readonly<Record<string, string>> = {}) {
        this.map = new Map();

        for (const [name, value] of Object.entries(headers)) {
            this.set(name, value);
        }
    }

    private getKey(name: string): null | string {
        name = name.toLowerCase();

        for (const key of this.map.keys()) {
            if (name === key.toLowerCase()) {
                return key;
            }
        }

        return null;
    }

    public [Symbol.iterator](): Iterator<[string, string]> {
        return this.map[Symbol.iterator]();
    }

    public has(name: string): boolean {
        const key: null | string = this.getKey(name);
        const has: boolean = Check.isNotNull(key);

        return has;
    }
    public hasNot(name: string): boolean {
        const has: boolean = this.has(name);
        const hasNot: boolean = Check.isFalse(has);

        return hasNot;
    }

    public get(name: string): string {
        const key: null | string = this.getKey(name);

        if (Check.isNull(key)) {
            throw new Error(`Name '${name}' does not exist.`);
        }

        const value: string = this.map.get(key) as string;

        return value;
    }

    public set(name: string, value: string | ReadonlyArray<string>): void {
        if (Check.isZero(value.length)) {
            throw new Error('The value must not be an empty string or an empty array.');
        }

        if (Check.isArray(value)) {
            value = value.join(', ');
        }

        const key: string = this.getKey(name) || name;

        this.map.set(key, value);
    }

    public append(name: string, value: string | ReadonlyArray<string>): void {
        const key: null | string = this.getKey(name);

        if (Check.isZero(value.length)) {
            throw new Error('The value must not be an empty string or an empty array.');
        }

        if (Check.isNull(key)) {
            this.set(name, value);
        }
        else {
            if (Check.isArray(value)) {
                value = value.join(', ');
            }

            const previousValue: string = this.map.get(key) as string;
            const values: ReadonlyArray<string> = [previousValue, value];
            const joinedValues: string = values.join(', ');

            this.map.set(key, joinedValues);
        }
    }

    public delete(name: string): void {
        const key: null | string = this.getKey(name);

        if (Check.isNull(key)) {
            return;
        }

        this.map.delete(key);
    }
    public deleteAll(names: Iterable<string> = this.map.keys()): void {
        for (const name of names) {
            this.delete(name);
        }
    }
}
export interface ReadonlyHttpHeaders extends Iterable<[string, string]> {
    has(name: string): boolean;
    hasNot(name: string): boolean;
    get(name: string): string;
}
