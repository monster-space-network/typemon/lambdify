import { ParameterMetadata, Container, Identifier } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
import { ParameterToken } from './parameter-token';
import { HttpRequest, ReadonlyHttpHeaders, ReadonlyHttpParameters, HttpResponse, HttpHeaders, HttpSharedContext } from './http';
//
//
//
export namespace ParameterResolver {
    export async function resolve(metadata: ParameterMetadata, container: Container): Promise<unknown> {
        const identifier: Identifier = metadata.identifier || metadata.type;

        switch (identifier) {
            case ParameterToken.HEADERS: {
                const request: HttpRequest = await container.get(HttpRequest);
                const headers: ReadonlyHttpHeaders = request.headers;

                return headers;
            }
            case ParameterToken.HEADER: {
                const name: string = metadata.get(MetadataKey.HeaderName);
                const request: HttpRequest = await container.get(HttpRequest);
                const headers: ReadonlyHttpHeaders = request.headers;
                const header: null | string = headers.has(name)
                    ? headers.get(name)
                    : null;

                return header;
            }

            case ParameterToken.PATH_PARAMETERS: {
                const request: HttpRequest = await container.get(HttpRequest);
                const parameters: ReadonlyHttpParameters = request.pathParameters;

                return parameters;
            }
            case ParameterToken.PATH_PARAMETER: {
                const key: string = metadata.get(MetadataKey.ParameterKey);
                const request: HttpRequest = await container.get(HttpRequest);
                const parameters: ReadonlyHttpParameters = request.pathParameters;
                const parameter: null | string = parameters.has(key)
                    ? parameters.get(key)
                    : null;

                return parameter;
            }

            case ParameterToken.QUERY_PARAMETERS: {
                const request: HttpRequest = await container.get(HttpRequest);
                const parameters: ReadonlyHttpParameters = request.queryStringParameters;

                return parameters;
            }
            case ParameterToken.QUERY_PARAMETER: {
                const key: string = metadata.get(MetadataKey.ParameterKey);
                const request: HttpRequest = await container.get(HttpRequest);
                const parameters: ReadonlyHttpParameters = request.queryStringParameters;
                const parameter: null | string = parameters.has(key)
                    ? parameters.get(key)
                    : null;

                return parameter;
            }

            case ParameterToken.BODY: {
                const request: HttpRequest = await container.get(HttpRequest);
                const body: unknown = request.body;

                return body;
            }

            case ParameterToken.RESPONSE_HEADERS: {
                const response: HttpResponse = await container.get(HttpResponse);
                const headers: HttpHeaders = response.headers;

                return headers;
            }

            case HttpSharedContext: {
                const sharedContext: HttpSharedContext = await container.get(HttpSharedContext);

                if (metadata.hasNot(MetadataKey.SharedContextKey)) {
                    return sharedContext;
                }

                const key: string = metadata.get(MetadataKey.SharedContextKey);
                const value: unknown = sharedContext.get(key);

                return value;
            }

            default:
                return container.get(identifier, metadata.flag);
        }
    }

    export async function resolveAll(metadatas: ReadonlyArray<ParameterMetadata>, container: Container): Promise<ReadonlyArray<unknown>> {
        const values: Array<unknown> = [];

        for (const metadata of metadatas) {
            const value: unknown = await resolve(metadata, container);

            values.push(value);
        }

        return values;
    }
}
