import { Inject } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { ParameterToken } from './parameter-token';
//
//
//
export type Event<Type extends object = any> = Type;
export function Event(): ParameterDecorator {
    return (target: Object, propertyKey: undefined | string | symbol, index: number): void => {
        if (Check.isUndefined(propertyKey)) {
            throw new Error(`Decorator 'Event' can not be used in the constructor.`);
        }

        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Event' can not be used for symbol properties.`);
        }

        Inject.decorate(ParameterToken.EVENT, target, propertyKey, index);
    };
}
