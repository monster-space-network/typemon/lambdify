//
//
//
export enum MetadataKey {
    HeaderName = 'typemon:lambdify:parameters:header:name',
    ParameterKey = 'typemon:lambdify:parameters:parameter:key',
    SharedContextKey = 'typemon:lambdify:parameters:shared-context:key'
}
