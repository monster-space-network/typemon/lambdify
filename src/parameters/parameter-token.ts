import { InjectionToken } from '@typemon/dependency-injection';
//
//
//
export namespace ParameterToken {
    export const EVENT: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Event');
    export const CONTEXT: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Context');

    export const HEADERS: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Headers');
    export const HEADER: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Header');

    export const PATH_PARAMETERS: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Path-Parameters');
    export const PATH_PARAMETER: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Path-Parameter');

    export const QUERY_PARAMETERS: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Query-Parameters');
    export const QUERY_PARAMETER: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Query-Parameter');

    export const BODY: InjectionToken = InjectionToken.create('Typemon.Lambdify.Parameters.Body');

    export const NEXT: InjectionToken = InjectionToken.create('Tyepmon.Lambdify.Parameters.Next');

    export const RESPONSE_HEADERS: InjectionToken = InjectionToken.create('Tyepmon.Lambdify.Parameters.Response-Headers');
}
