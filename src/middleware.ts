import { Provider, Constructor, TargetHandle } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
//
//
//
export interface Middleware {
    handler(...parameters: Array<any>): void | Promise<void>;
}
export function Middleware(options: Middleware.Options = {}): ClassDecorator {
    return (target: Function): void => {
        const constructor: Constructor = TargetHandle.targetify(target);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);

        if (targetHandle.hasMetadata(MetadataKey.MiddlewareMetadata)) {
            throw new Error(`Decorator 'Middleware' can not be duplicated.`);
        }

        const metadata: Middleware.Metadata = {
            options
        };

        targetHandle.setMetadata(MetadataKey.MiddlewareMetadata, metadata);
    };
}
export namespace Middleware {
    export interface Options {
        readonly providers?: ReadonlyArray<Provider>;
    }
    export interface Metadata {
        readonly options: Options;
    }

    export type Constructor = new (...parameters: Array<any>) => Middleware;

    export class WithProviders {
        public static isWithProviders(value: unknown): value is WithProviders {
            return value instanceof WithProviders;
        }

        public static create(target: Constructor, providers: ReadonlyArray<Provider>): WithProviders {
            return new WithProviders(target, providers);
        }

        public constructor(
            public readonly target: Constructor,
            public readonly providers: ReadonlyArray<Provider>
        ) { }
    }
}
