import { Provider } from '@typemon/dependency-injection';
//
import { Middleware } from './middleware';
//
//
//
export interface GlobalOptions {
    /**
     * @default true
     */
    readonly useAPIGateway?: boolean;
    readonly providers?: ReadonlyArray<Provider>;
    readonly middlewares?: ReadonlyArray<Middleware.Constructor | Middleware.WithProviders>;
}
