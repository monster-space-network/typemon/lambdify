import { Provider, Constructor, TargetHandle } from '@typemon/dependency-injection';
import { Check } from '@typemon/check';
//
import { MetadataKey } from './metadata-key';
import { Middleware } from './middleware';
//
//
//
export function Handler(options: Handler.Options = {}): MethodDecorator {
    return (target: Object, propertyKey: string | symbol): void => {
        if (Check.isSymbol(propertyKey)) {
            throw new Error(`Decorator 'Handler' can not be used for symbol properties.`);
        }

        const constructor: Constructor = TargetHandle.targetify(target, propertyKey);
        const targetHandle: TargetHandle = TargetHandle.create(constructor);
        const metadatas: Map<string, Handler.Metadata> = targetHandle.hasMetadata(MetadataKey.HandlerMetadatas)
            ? targetHandle.getMetadata(MetadataKey.HandlerMetadatas)
            : new Map();

        if (metadatas.has(propertyKey)) {
            throw new Error(`Decorator 'Handler' can not be duplicated.`);
        }

        if (Check.isNotUndefined(options.name)) {
            for (const metadata of metadatas.values()) {
                if (Check.isNotUndefined(metadata.options.name) && metadata.options.name === options.name) {
                    throw new Error(`Name '${options.name}' already exists.`);
                }
            }
        }

        const metadata: Handler.Metadata = {
            propertyKey,
            options
        };

        metadatas.set(propertyKey, metadata);
        targetHandle.setMetadata(MetadataKey.HandlerMetadatas, metadatas);
    };
}
export namespace Handler {
    export interface Options {
        /**
         * @default KebabCase.convert(propertyKey)
         */
        readonly name?: string;

        /**
         * @default true
         */
        readonly useAPIGateway?: boolean;

        /**
         * @default 200
         */
        readonly statusCode?: number;

        readonly headers?: Readonly<Record<string, string>>;

        readonly providers?: ReadonlyArray<Provider>;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor | Middleware.WithProviders>;
    }
    export interface Metadata {
        readonly propertyKey: string;
        readonly options: Options;
    }
}
