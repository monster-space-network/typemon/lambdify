import { Constructor, TargetHandle } from '@typemon/dependency-injection';
//
import { KebabCase } from './util';
import { MetadataKey } from './metadata-key';
import { GlobalOptions } from './global-options';
import { Event, Context } from './parameters';
import { Handler } from './handler';
import { Runner } from './runner';
//
//
//
let globalOptions: GlobalOptions = {};

export type Lambda = (event: Event, context: Context) => Promise<unknown>;
export type Lambdas = Readonly<Record<string, Lambda>>;

export function lambdify(target: Constructor): Lambdas {
    const targetHandle: TargetHandle = TargetHandle.create(target);

    if (targetHandle.hasNotMetadata(MetadataKey.ControllerMetadata)) {
        throw new Error(`Decorator 'Controller' is required.`);
    }

    if (targetHandle.hasNotMetadata(MetadataKey.HandlerMetadatas)) {
        throw new Error(`There are no handlers in the controller.`);
    }

    const lambdas: Record<string, Lambda> = {};
    const handlerMetadatas: ReadonlyMap<string, Handler.Metadata> = targetHandle.getMetadata(MetadataKey.HandlerMetadatas);

    for (const handlerMetadata of handlerMetadatas.values()) {
        const propertyKey: string = handlerMetadata.propertyKey as string;
        const name: string = handlerMetadata.options.name || KebabCase.convert(propertyKey);
        const runner: Runner = new Runner(target, propertyKey, globalOptions);
        const lambda: Lambda = (event: Event, context: Context): Promise<unknown> => runner.run(event, context);

        lambdas[name] = lambda;
    }

    return lambdas;
}
export namespace lambdify {
    export function globally(options: GlobalOptions): void {
        globalOptions = options;
    }
}
