//
//
//
export namespace KebabCase {
    export function convert(value: string): string {
        const converted: string = value
            .replace(/([a-z])([A-Z])/g, '$1-$2')
            .toLowerCase();

        return converted;
    }
}
